// Copyright (c) 2017 Able Systems Ltd.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the FreeBSD Project.

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import org.cups4j.CupsClient;
import org.cups4j.CupsPrinter;
import org.cups4j.PrintJob;
import org.cups4j.PrintJob.Builder;
import org.cups4j.PrintRequestResult;
import java.net.URL;
import java.lang.String;
import java.nio.charset.StandardCharsets;

public class PipstaPrinter {
    private static byte ESC = 0x1b;
    private static byte GS = 0x1d;

    public static byte[] ean13Barcode() {
        return new byte[] {
                GS, 'w', 0x03, // barcode width (default)
                GS, 'H', 0x03, // print barcode text above and below barcode
                ESC, '!', (byte)0x80, // set font mode 0 with underscore
                'E', 'x', 'a', 'm', 'p', 'l', 'e', ' ', 'o', 'f', ' ', 'E', 'A', 'N', '1', '3', '\r', // print text
                ESC, '!', 0x00, // font mode 0, no underline
                GS, 'k', 0x02, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', 0x00, // print barcode
                '\r'
        };
    }

    public static void main(String[] argv) {
        try {
            // Connect to the CUPS subsystem
            CupsClient cupsClient = new CupsClient();

            if (argv[0].equals("-list")) {
                // If the user asks for a list of printers then ask CUPS
                for (CupsPrinter p : cupsClient.getPrinters()) {
                    System.out.println(p.getPrinterURL());
                }

                return;
            }

            // Take the user supplied URL, if invalid exceptions are thrown
            URL url = new URL(argv[0]);
            CupsPrinter cp = cupsClient.getPrinter(url);

            // Set the printer to raw mode
            HashMap<String, String> map = new HashMap<>();
            map.put("document-format", "application/vnd.cups-raw");

            // Build the raw comand stream
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write("Hello from Java\n".getBytes(StandardCharsets.US_ASCII));
            bos.write(ean13Barcode());

            // Submit the job to be printed
            PrintJob printJob = new PrintJob.Builder(bos.toByteArray()).attributes(map).build();
            PrintRequestResult printRequestResult = cp.print(printJob);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}

