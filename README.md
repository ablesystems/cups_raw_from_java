# README #

This is an initial example showing how to connect to an Ap1400 in RAW mode via CUPS.  For examples of printing normally via CUPS see [Printing Using CUPS](https://bitbucket.org/ablesystems/pipsta/wiki/Printing%20Using%20CUPS).

### What is this repository for? ###

It a basic example of how to connect to your CUPS server using JAVA and send raw commands to the printer.  The example uses a very small subset of the Ap1400/Pipsta command set.  For the printers programmer guide please contact [support@able-systems.com](mailto:support@able-systems.com).

### How do I get set up? ###

* Download the [CUPS printer driver](https://bitbucket.org/ablesystems/pipsta/downloads/pipsta-cups-driver-0.3.0-armhf.deb)
* Disconnect the Ap1400 printer
* Install the printer driver `$ sudo apt-get update && sudo dpkg -i pipsta-cups-driver-0.3.0-armhf.deb && sudo apt-get -f install`
* Reconnect the Ap1400 printer
* Get a copy of the [cups4j](http://www.cups4j.org/) JAR file.
* Build `$ javac -cp cups4j.runnable-0.6.4.jar PipstaPrinter.java`
* Run `$ java -cp .:cups4j.runnable-0.6.4.jar PipstaPrinter -list`

If the setup has worked you should see a list of printers.  In this list you should
see an Ap1400 and a Pipsta.  To identify which printer you have you can initiate a demo print by double-pressing the printers feed button.  The printout will inform you of the type (Ap1400|Pipsta).

Now the application can do it's demonstration print -
```
$ java -cp .:cups4j.runnable-0.6.4.jar PipstaPrinter http://localhost:631/printers/Ap1400
```

### Contribution guidelines ###

* Nothing yet, merely submit a pull request to initiate communications.
* If you wish to supply any other examples please feel free to get in touch.
* If you would like us to look into any other examples, again, please get in touch.

### Who do I talk to? ###

[support@able-systems.com](mailto:support@able-systems.com)